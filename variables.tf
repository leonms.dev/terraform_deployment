variable "project_id" {
  default = "leomsdev-terraform"
}

variable "bucket" {
  default = "state-leonmssdev"
}
