terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "4.68.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "3.4.1"
    }
  }

    backend "gcs" {
    bucket = "state-leonmssdev"
    prefix = "terraform/state"
  }
}

provider "google" {
  project = var.project_id
  region  = "europe-west4"
}

provider "random" {
  # Configuration options
}