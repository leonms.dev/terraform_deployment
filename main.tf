output "project_id" {
  value = var.project_id
}

resource "random_pet" "pet" {
  keepers = {
    ami_id = "124ffx"
  }
  length = 1
}

module "gcs_buckets" {
  source  = "terraform-google-modules/cloud-storage/google"
  version = "3.4.0"
  project_id  = var.project_id
  names = ["first"]
  prefix = random_pet.pet.keepers.ami_id
  set_admin_roles = false
  versioning = {
    first = true
  }
}